// Script for /details/index.html

document.addEventListener('DOMContentLoaded', function () {
	// Add 'flipped' class to the flipper to engage animation.
	window.setTimeout(function() {
		var flipper = document.getElementById('dm-cc-flipper');
		flipper.className = flipper.className + " flipped";
	}, 1);

	document.getElementById('flip-to-front-icon').addEventListener("click", function(event) {
		event.preventDefault();
		var flipper = document.getElementById('dm-cc-flipper');
		flipper.className = flipper.className.replace(" flipped", "");
		window.setTimeout(function() {
			window.location = document.getElementById('flip-to-front-icon').href;
		}, 350);
	});
});
