/*
 * Gulpfile for building datamadsen.dk - output is placed in `outdir` folder.
 */
'use strict'

var outdir = '_build/';

var gulp         = require('gulp');
var gutil        = require('gulp-util');
var sass         = require('gulp-sass')
var livereload   = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var express      = require('express');

// Compile .scss files in styles and place output in `outdir`
gulp.task('sass', function() {
	return gulp.src('styles/**/*.scss')
		.pipe(sass({ outputStyle: 'compressed' }))
		.pipe(autoprefixer('last 1 version'))
		.pipe(gulp.dest(outdir + 'css'));
});

// Copy pages to `outdir`
gulp.task('pages', function() {
	return gulp.src('pages/**/*').pipe(gulp.dest(outdir));
});

// Copy scripts to `outdir`
gulp.task('scripts', function() {
	return gulp.src('scripts/**/*').pipe(gulp.dest(outdir + 'scripts'));
});

// Copy images to `outdir`
gulp.task('images', function() {
	return gulp.src('images/**/*').pipe(gulp.dest(outdir + 'images'));
});

// Copy files to `outdir`
gulp.task('files', function() {
	return gulp.src('files/**/*').pipe(gulp.dest(outdir + 'files'));
});

// Copy administrativia to `outdir`
gulp.task('administrativia', function() {
	return gulp.src('administrativia/**/*').pipe(gulp.dest(outdir));
});

// Task for watching for changes, and running tasks based on those changes.
// Changes in the build folder results in a reload for the affected files.
gulp.task('serve', ['build', 'watch'], function() {
	var app = express();
	app.use(express.static(outdir));
	app.listen(8080);
	livereload.listen();

	gulp.watch('_build/**/*', function(file) {
		// Remove stuff from file path to get relative url.
		// file path example: ~/src/project/_build/path/to/index.html
		var relative_url = file.path.match(/_build(.*)/)[1];
		relative_url = relative_url.replace(/index.html$/, "");

		livereload.changed(relative_url);
	});

	gutil.log(gutil.colors.blue('Listening on http://localhost:8080'));
	gutil.log(gutil.colors.blue('Use the livereload plugin in your browser'));
});

gulp.task('watch', function() {
	gulp.watch('styles/**/*.scss', ['sass']);
	gulp.watch('pages/**/*', ['pages']);
	gulp.watch('scripts/**/*', ['scripts']);
	gulp.watch('images/**/*', ['images']);
	gulp.watch('files/**/*', ['files']);
	gulp.watch('administrativia/**/*', ['administrativia']);
});

gulp.task('build', function() {
	gulp.start('sass');
	gulp.start('pages');
	gulp.start('scripts');
	gulp.start('images');
	gulp.start('files');
	gulp.start('administrativia');
});

// Default task: build and watch.
gulp.task('default', function() {
	gutil.log(gutil.colors.blue("USAGE"));
	gutil.log(gutil.colors.blue("\t\tbuild \t output is placed in " + outdir));
	gutil.log(gutil.colors.blue("\t\tserve \t build, serve, and watch for changes."));
});

