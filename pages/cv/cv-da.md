title: CV: Tim Mølgaard Madsen, Sofware Ingeniør, tim@datamadsen.dk
date:  August, 2015

# Baggrund
Som softwareingeniør (Cand.polyt. ved Aalborg Universitet) har Tim
specialiseret sig i at bygge både nye selvstændige systemer samt systemer til
integration af data mellem allerede eksisterende systemer. Tim er uddannet i
mange programmeringsteknologier og udviklingsmetoder og er en problemknuser med
en løsningsorienteret tankegang.

Tim sætter en ære i veludført arbejde, arbejder selvstændigt med drive og
entusiasme, og er hurtig til at opnå detaljeret viden omkring både
forretningsområder og teknologier han beskæftiger sig med. 

# Relevant erhvervserfaring

## Danske Bank / august 2015 - juli 2016
Tim har været ekstern konsulent hos Danske Banks Group Performance Management
afdeling, hvor han har haft til opgave at lave et ny system til at håndtere
Service Level Agreements på produkter som bliver udvekslet mellem afdelinger
internt i banken.

Systemet underbygger de forretningsprocesser som er defineret i forhold til at
forhandle kontrakterne på plads, inklusive elektronisk godkendelse fra de
respektive parters ledelse.

### Teknologier
C#, .NET, ASP.NET MVC, Entity Framework, MS SQL Server, T-SQL, Unit Testing,
NUnit, IIS (opsætning og drift), HTML(5), JavaScript, CSS(3), git.

### Arbejdsopgaver
  * Design og implementering af nyt system til at håndtere SLAer i Danske Bank.
    Tim har været ene udvikler på projektet og har varetaget både backend- og
    frontednudvikling. Backenden er skrevet i C#/.NET vha. ASP.NET MVC
    frameworket. Frontenden er skrevet i HTML, CSS og JavaScript og bliver
    renderet serverside vha. Razor. Webapplikationen kører på en IIS 8.0, som
    Tim har stået for opsætning af. Systemets datastore er en MS SQL Database
    leveret af den interne organisation, som blot har brugt.

  * Løbende dialog med forretningen for at afstemme forventninger og afklare
    behov og krav til systemet, hvis første version blev udgivet 1. januar 2016
    med løbende forbedringer i 2016.

  * Konvertering af allerede eksisterende aftaler i et gammelt system over i det
    nyudviklede system.

  * Integration til informationer om Danske Banks HR og organisationsstruktur.
    Informationer om medarbejdere og deres tilknytninger til afdelinger bliver
    løbende importeret til systemet for at relevante personer har adgang til
    SLAer relevante for dem.

  * Integration til Danske Banks produktkatalog som danner grundlaget for en
    SLA. Produktkataloget i SLA systemet bliver løbende holdt ajour med det
    system hvori produkterne vedligeholdes.

  * Tim har skrevet unit tests til store dele af systemet.

## KMD / oktober 2014 - juli 2015
Tim har fungeret som ekstern konsulent i KMDs division for Borgernære
Softwareløsninger, hvor han har udviklet en borgervendt selvbetjeningsløsning
som gør det muligt at ansøge om godtgørelse for transportudgifter i forbindelse
med eksempelvis lægebesøg. Tim har været ene C# udvikler på projektet, og har
arbejdet sammen med polske udviklere som har løsningens SAP-komponent og danske
forretningsudviklere som har stillet opgaven.

### Teknologier
HTML(5), JavaScript, CSS(3), bootstrap, jQuery, RequireJS, C#, .NET, ASP.NET
MVC, Unit Testing, NUnit, SAP integration, Borger.dk.

### Arbejdsopgaver
  * Udvikling af domænemodeller i C#/.NET som bl.a.  understøtter valideringer
    involveret i ansøgningsprocessen.

  * Udvikling af konfigurationsværktøj som gør det muligt at definere
    parametrene for bl.a. valideringsregler i en backend.
    Selvbetjeningsløsningen er reelt set et framework, som kan konfigureres for
    hver kommune vha. SAP. Tim har dokumenteret mulighederne så KMDs
    driftspersonale kan håndtere konfigurationen.

  * Udvikling af frontend i html5, css, and javascript.
    Frontenden gør det bl.a. muligt at dokumentere udgifter
    vha. kamera i telefon eller computer. Funktionaliteten er
    kompatibel med Internet Explorer 8 og bedre.

  * Udvikling af C#/.NET backend integrationer med SAP, som er
    det system kommunernes sagsbehandler behandler ansøgninger i.

  * Integration med borger.dk og NemID.

  * Har yderligere fungeret som buddy og mentor for nyansat KMD
    medarbejder.

## Mailreminder.net / juli 2014 - ?
Mailreminder.net er en service, som gør det nemt for dens brugere at blive
mindet om en given mail på et givet tidspunkt uden at servicen på noget
tidspunkt ser mailens indhold - "privacy" er i højsædet. Kan f.eks. bruges til
at få flybiletter øverst i inboxen den dag man skal rejse.

### Teknologier
HTML(5), JavaScript, Backbone, CSS(3), underscore, jQuery, RequireJS, grunt,
gulp, livereload, FreeBSD, Node.js, Express, MongoDB, PostgreSQL, SSL, postfix,
Dovecot, NGINX, supervisord, test-driven development.

### Arbejdsopgaver
Tim har udviklet en backend til mailreminder.net, som består af:

  * Et REST API, skrevet i JavaScript vha. Node.js, Express og MongoDB. API'et
    sender og modtager JSON strukturer.

  * En mail server i form af postfix (MTA) og dovecot (IMAP) som er
    konfigureret med konti defineret i en PostgreSQL database.

  * En mail klient, skrevet i JavaScript vha. Node.js, som kontrollerer
    indkomne mails og sætter en reminder op for det angivne tidspunkt.

  * Et jobafviklingsværktøj skrevet i JavaScript vha. Node.js som sørger for at
    sende reminders ud på de korrekte tidspunkter.

Tim har også udviklet en frontend, hvor brugere af servicen kan se hvilke
reminders der er sat op. Frontenden består af:

  * En "Single Page Application", skrevet i JavaScript i MVC mønstret vha.
    Backbone.js, underscore.js, RequireJS og underscore.js templates.

  * Styling i form af Sass stylesheets som bliver kompileret til css.

  * Build scripts som bl.a. minifier/uglifier kode og optimerer billeder for at
    gøre loadtider så korte som muligt.

Alle services krævet for at køre mailreminder.net afvikles på en FreeBSD server
som Tim også vedligeholder.

## NHL Data / juni 2014 - juli 2014
Tim har været ekstern konsulent hos NHL Data i forbindelse med opstart af
frontend til et nyt produkt.

### Teknologier
HTML(5), CSS(3), JavaScript, Backbone, underscore, jQuery, requirejs, grunt,
Live reload, C# .NET webservices, Scrum.

### Arbejdsopgaver
Tim har rådgivet omkring strukturering af JavaScript applikation til et af NHLs
nye produkter. Tim har:

  * Udviklet skeletapplikation i MVC/MVT mønstret vha. Backbone.js,
    underscore.js, og RequireJS. NHLs udviklere har brugt skelettet til
    inspiration og videre udvikling.

  * Udvikling af underscore templates.

  * Udvikling af integrationer med c#/.net backend vha. json strukturer.

## Danske Bank / august 2013 - maj 2014
Tim har været ekstern konsulent hos Danske Bank Credit Management. Her har Tim
været med til at udvikle en web application til et nyt "impairment" system,
hvor sagsbehandlere kan opstille en række scenraier for mulige udfald af
eksempelvis en kundes konkurs og deres konsekvenser for banken.

### Teknologier
C#, .NET, ASP.NET, HTML, CSS, JS (jQuery, Backbone og underscore), Live reload,
Unit test, integration til mainframe, Scrum.

### Arbejdsopgaver
  * Udvikling af frontend i HTML, CSS, og JavaScript som kommunikerer med en
    C#/.NET backend vha. AJAX. I frontenden er det muligt for sagsbehandlere at
    indtaste forventede tab på bl.a. en række konti og lån under visse
    omstændigheder (scenarier) og samtidig se de samlede konsekvenser for
    banken.  For at gøre brugeroplevelsen så gnidningsfri som muligt har Tim
    gjort brug af en række frontend frameworks (jQuery, Backbone, Underscore og
    flere).

  * Udvikling af en C#/.NET backend applikation som kommunikerer med frontend
    og en række mainframe programmer vha. webservices for bl.a. at gøre det
    muligt, i takt med indtastning, at vise sagsbehandleren økonomiske
    konsekvenser for banken ved en kundes konkurs.

  * Udvikling af integrationer til dokumentgenerering på baggrund af indtastede
    oplysninger. Dokumenter anvendes i forbindelse med ledelsesrapportering og
    bogføring.

  * Specificering af webservices som skal udstilles af mainframe for at opfylde
    Danske Banks krav til en flydende brugeroplevelse.

  * Specificering af opgaver til Indisk udvikler, som har arbejdet på en anden
    del af brugergrænsefladen.

  * Teknisk dokumentation af hele stakken: C#/.NET, HTML, CSS, JavaScript,
    webservices, og integration til mainframe.

## Edlund / februar 2013 - august 2013
Tim har været ansat hos Edlund, hvor han har arbejdet med INPAS, som er Edlunds
standardsystem til non-life forsikring. Tim har haft opgaver indenfor design og
udvikling på reguleringsforsikringer, og har også arbejdet med analyse, design
og udvikling/rettelser til INPAS webservices som blev brugt til at konvertere
nye kunders data ind i systemet.

### Teknologier
C#, .NET, T-SQL, Windows Presentation Foundation, Reflection, NUnit, Unit test,
Test-Driven Development, Remoting, Scrum.

### Arbejdsopgaver
  * Udvikling af datamodel som er grundlaget for reguleringsforsikringer i
    C#/.NET, T-SQL, .NET remoting, samt WPF. Reguleringsforsikringer anvendes
    bl.a. af virksomheder med en svingende lagerbeholdning, hvorfor deres
    præmie skal reguleres løbende.

  * Analyse, design, og udvikling af webservices (og rettelser til webservices)
    som bruges i forbindelse med konvertering i C#/.NET. Edlund skulle
    konvertere 7 nye skadesforsikringskunder over i INPAS - kunderne kom fra i
    alt 3 forskellige systemer.

  * Analyse, design og specifikation af opgaver til Srilankansk udviklingsteam
    med efterfølgende test af det leverede.

  * Test af både reguleringsforsikringer og konverteringssoftware vha. unit
    testing og integrationstest.

  * Teknisk dokumentation af reguleringsforsikringer. 

## Boligkontoret Danmark / februar 2012 - februar 2013
Konsulent hos Netcompany: Analyse, design, udvikling af ESDH system til BDK som
har udvidet opgavestyring og integration til bestyrelses- og
selvbetjeningsportaler.

### Teknologier
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Arbejdsopgaver
  * Udvikling af konverteringsværktøjer i C#/.NET til brug for konvertering af
    afdelinger, opnoterede, lejere, og andre interessenter.

  * Udvikling af SharePoint webparts i HTML, CSS, JS og ASP.NET.

  * Integration af ERP data i SharePoint som anvendes i forbindelse med sags-
    og dokument taksonomi samt opgavetildeling.

  * Dimensionering og opsætning af server infrastruktur til test- og
    produktionsmiljøer med MS SQL servere, SharePoint servere og loadbalancing.

  * Undervisning af ca. 30 både tekniske og forretningsorienterede
    nøglepersoner i produktets funktioner og anvendelse med henblik på
    forankring i organisationen.

## Københavns Almennyttige Boligselskab / januar 2012 - februar 2013
Konsulent hos Netcompany: Analyse, design, udvikling af ESDH system til KAB som
bla. integrerer med KABs Oracle ERP system og selvbetjeningsportaler. KAB er en
non-profit boligorganisation som varetager driften af ca. 1000 boligafdelinger
fordelt på ca. 500 boligselskaber, som huser omkring 50.000 lejere og deres
familier.

### Teknologier
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Arbejdsopgaver
  * Udvikling af integration til Oracle ERP system i C#/.NET vha. webservices.
    Integration af systemets oplysninger i SharePoint, hvor de er tilgængelige
    i forbindelse med bl.a. sags- og dokument taksonomi.

  * Udvikling af opgavesystem i C#/.NET og SharePoint, hvor opgaveskabeloner
    definerer hvilken rolle en given opgave skal tildeles. ERP oplysninger
    anvendes til at bestemme hvilken person, der opfylder hvilken rolle i en
    given boligafdeling.

  * Udvikling af SharePoint webparts i HTML, CSS, JavaScript, og ASP.NET, hvor
    det bl.a. er muligt at se en rollebesætning for en given afdeling.

  * Udvikling af integrationer til KABs selvbetjeningsportaler, hvor det bl.a.
    er muligt at få leveret post elektronisk i forbindelse med
    bestyrelsesarbejde.

  * Dimensionering og opsætning af server infrastruktur til test- og
    produktionsmiljøer med MS SQL servere, SharePoint servere og loadbalancing.

## Danmarks Almennyttige Boligselskab / oktober 2011 - januar 2012
Konsulent hos Netcompany: DAB har i forlængelse af deres ESDH implementering
ønsket at fremme samarbejdet for medlemmer af deres afdelings- og
selskabsbestyrelser. Ønsket er realiseret ved en SharePoint portal, hvor
bestyrelser kan logge ind med NemID og samarbejde på projekter. DAB anvender
bl.a. også portalen til at publicere breve til bestyrelser – en funktion der
sparer ca. 1 million kroner i porto, årligt. 

### Teknologier
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint

### Arbejdsopgaver
  * Udvikling af integrationer til økonomisystem i C#/.NET vha. webservices,
    for bl.a. at gøre det muligt for bestyrelsesmedlemmer at gennemse
    kontiposteringer.

  * Udvikling af brugergrænseflade til visning af kontiposteringer i HTML, CSS,
    JavaScript og ASP.NET.

  * Udvikling af værktøjer til oprettelse af "bestyrelsesrum" som opfylder
    fortroligshedskrav om at DAB ikke må have adgang til bestyrelsens
    dokumenter og sager.

  * Oplæring af 3 nye medarbejdere i tekniske aspekter af produktet.

## DAHL Advokater / august 2011 - oktober 2011
Konsulent hos Netcompany: DAHL Advokater er et af Danmarks største
advokatfirmaer med ca. 200 medarbejdere. DAHL har i samarbejde med EG Data
Inform udarbejdet en vision for DAHLs fremtidige ESDH system. 

### Teknologier
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint

### Arbejdsopgaver
  * Tim har ud fra en kravspecifikation udarbejdet prototype og
    løsningsbeskrivelse på det beskrevne system for at sparke projektet i gang. 

  * Udvikling af prototype med proof of concept integrationer til Navision
    system i C#/.NET.

  * Specificering af udviklingsopgaver, som er uddelegeret til Senior
    Developer, der har spurgt Tim til råds undervejs i forløbet i forbindelse
    med C#/.NET, HTML, CSS, JavaScript og SharePoint.

  * Undervisning af ca. 20 personer i den udviklede prototype for at synliggøre
    systemets funktioner og anvendelse.

## ACCURA / juni 2011 - juni 2011
Konsulent hos Netcompany: ACCURA rådgiver en lang række store nationale og
multinationale erhvervskunder, finansielle institutioner, private equity-fonde,
statsejede investeringsfonde, forsyningsselskaber, offentlige myndigheder,
regeringer samt succesrige private selskaber, deres ejere og visse personer med
stor nettoformue. ACCURA har en ESDH løsning som håndterer viden genereret ved
rådgivningen. 

### Teknologier
C#, .NET, T-SQL, HTML, CSS, JS, SharePoint

### Arbejdsopgaver
Efter idriftsættelse af ACCURAs ESDH løsning opstod behov for diverse
tilpasninger, som Tim har designet, og udviklet. Projektets udviklere faldt
bagud i forhold til planen og Tims hovedfunktion på projektet bestod i at
indhente tabt tid for at imødekomme aftalt deadline.

## Dansk Almennyttigt Boligselskab / oktober 2010 - oktober 2011
Konsulent hos Netcompany: DAB er en non-profit boligorganisation, som varetager
driften af ca. 100 boligselskaber med ca. 1.000 afdelinger, der huser ca.
150.000 lejere og deres familier. 

Tim har været drivkraften i udviklingen af GO Bolig; en overbygning til
GetOrganized ESDH-systemet, som gør det muligt for DAB at behandle sager
billigere og hurtigere med bedre service for de mange afdelinger og beboere. 

### Teknologier
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Arbejdsopgaver
  * Tilvejebringelse af krav gennem en række workshops, hvor Tim bl.a. har
    stået for udarbejdelse af præsentationsmateriale og mødereferater.

  * Udvikling af integrationer til ERP system i C#/.NET, der leverer
    oplysninger til brug i forbindelse med bl.a. sags- og dokument taksonomi.

  * Specificering, udvikling og test af opgavetildelingssystem i C#/.NET, som
    gør det muligt at tildele opgaver på sagsgange for boligafdelingers
    rollebesætning.

  * Udvikling af webparts til SharePoint i HTML, CSS, JavaScript og ASP.NET,
    hvor det bl.a. er muligt at få overblik over rollebesætning for en given
    boligafdeling.

  * Udvikling af værktøj til konvertering af afdelinger, opnoterede, medlemmer,
    og lejere i C#/.NET.

  * Systemet er forholdsvist kompleks og har presset SharePoint til grænserne,
    hvorfor Tim har stået for identificering og rettelser på svært
    identificerbare problemer i SharePoint.

  * Dimensionering og opsætning af server infrastruktur til test- og
    produktionsmiljøer med MS SQL servere, SharePoint servere og load
    balancing.

## Aalborg Universitet / juli 2008 - august 2008
Aalborg universitet ønskede en prototype på styring af Home Automation Systemer
vha. bl.a. en telefons GPS placering. I 2008 var Home Automation Systemer ikke
en ting man styrede vha. et touch interface - hvis det foregik på telefonen var
det vha. gammeldags sms - som regel foregik det vha. fjernbetjeninger. Tim
udarbejdede et REST API, som udstillede et indkøbt home automation systems
funktionaliteter. Arbejdet blev et startsted for Tims speciale i
programmeringsteknologier og indlejrede systemer på universitetet.

# Uddannelse

-------------------------------------------------------------------------------
Periode       Titel
------------- -----------------------------------------------------------------
2008 - 2010   Cand.Polyt. i Software (softwareingeniør), Aalborg Universitet.
							Speciale i programmeringsteknologier og indlejrede systemer. 

2004 - 2008   BSc i Teknisk Videnskab, Software, Aalborg Universitet.

2000 - 2003   HTX, Struer.

-------------------------------------------------------------------------------

# Anden erhvervserfaring

-------------------------------------------------------------------------------
Periode       Beskæftigelse
------------- -----------------------------------------------------------------
2004 - 2005   Værnepligtig ved ingeniørtropperne på Skive kaserne, PMV kører.

-------------------------------------------------------------------------------

# Sprog
Dansk og engelsk flydende i tale og på skrift.

# Kompetenceskema
+--------------+----------------------------------+-----------+----+---------+
| Kategori     | Kompetence                       | Niveau    | År | Senest  |
|              |                                  |           |    | anvendt |
+==============+==================================+===========+====+=========+
| Generelt     | Agile metoder                    | Rutineret | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Arkitektur                       | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Backend                          | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Brugergrænsefladeudvikling       | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Dokumentation                    | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | ESDH (Elektronisk sags- og       | Meget     | 3  | 2013    |
|              | dokumenthåndtering)              | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Forretningsdialog og workshops   | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Frontend                         | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Implementering                   | Rutineret | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Konvertering                     | Meget     | 4  | 2014    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Kravspecifikation                | Rutineret | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | OOA (Object Oriented Analysis)   | Rutineret | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | OOD (Object Oriented Design)     | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | OOP (Object Oriented             | Meget     | 5  | 2015    |
|              | Programming)                     | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Programmering                    | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Scrum                            | Rutineret | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Systemanalyse                    | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Systemdesign                     | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Systemudvikling                  | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Udviklingsmetoder                | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Web services                     | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Generelt     | Webudvikling                     | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | C#                               | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | CSS                              | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | HTML                             | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | JavaScript                       | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | Python                           | Rutineret | 1  | 2012    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | SQL                              | Rutineret | 4  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Sprog        | Transact-SQL                     | Rutineret | 4  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | .NET                             | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | Ajax                             | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | ASP.NET                          | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | LINQ                             | Meget     | 4  | 2014    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | Skade.Net (Edlund)               | Rutineret | 1  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | Windows Communication Foundation | Rutineret | 2  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | XML                              | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | Web                              | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | Web services                     | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | SharePoint 2010 (webparts,       | Ekspert   | 3  | 2013    |
|              | timerjobs, event handlers, mm.)  |           |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Teknologi    | REST                             | Meget     | 3  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Microsoft SQL Server             | Rutineret | 3  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Microsoft SQL Server 2008        | Rutineret | 3  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Backbone.js                      | Ekspert   | 2  | 2014    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Bootstrap                        | Rutineret | 3  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Gruntjs                          | Rutineret | 1  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | jQuery                           | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Microsoft Visual Studio          | Ekspert   | 5  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Node.js                          | Rutineret | 2  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Nunit                            | Meget     | 3  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Require.js                       | Meget     | 2  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | underscore.js                    | Ekspert   | 2  | 2015    |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | SharePoint 2010 (installation    | Ekspert   | 3  | 2013    |
|              | drift load balancing mm.)        |           |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | SVN / Git / TFS                  | Meget     | 5  | 2015    |
|              |                                  | rutineret |    |         |
+--------------+----------------------------------+-----------+----+---------+
| Udv. værktøj | Gemini                           | Godt      | 1  | 2014    |
|              |                                  | kendskab  |    |         |
+--------------+----------------------------------+-----------+----+---------+

<!-- vim: set ts=2 sw=2 tw=80 et : -->
