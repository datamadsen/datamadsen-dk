echo "Building CV..."
echo "\tDanish pdf...\t(1/4)"
pandoc -f markdown+mmd_title_block+multiline_tables -o cv-datamadsen-da.pdf \
	--latex-engine=xelatex --template templates/default.latex cv-da.md

echo "\tEnglish pdf...\t(2/4)"
pandoc -f markdown+mmd_title_block+multiline_tables -o cv-datamadsen-en.pdf \
	--latex-engine=xelatex --template templates/default.latex cv-en.md

echo "\tDanish docx... \t(3/4)"
pandoc -f markdown+mmd_title_block+multiline_tables -o cv-datamadsen-da.docx \
	cv-da.md

echo "\tEnglish docx...\t(4/4)"
pandoc -f markdown+mmd_title_block+multiline_tables -o cv-datamadsen-en.docx \
	cv-en.md

echo "CV build done."
