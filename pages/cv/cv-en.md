title: CV: Tim Mølgaard Madsen, Software Engineer, tim@datamadsen.dk
date:  August, 2015

# Background
As a software engineer (Cand.polyt. at Aalborg University), Tim has specialized
in building both new standalone software systems and systems that integrate data
from existing systems delivered by other vendors. Tim is skilled in many
programming technologies and software development methodologies, and is a
problem solver with a solution-oriented way of thinking.

Tim takes pride in a job well done, works independently with drive and
enthusiasm, and is quick to gain detailed knowledge about both technologies and
business domains that he’s involved with. 

# Relevant Work Experience

## Danske Bank / August 2015 - July 2016
Tim has been an external consultant at Danske Bank's Group Performance
Management department, where he has been responsible for developing a new system
for managing Service Level Agreements regarding products that are exchanged
between departments internally in the bank.

The system supports the standard operating procedures that have been defined for
negotiating the contracts, including electronic approval from the respective
departments' management.

### Technologies
C#, .NET, ASP.NET MVC, Entity Framework, MS SQL Server, T-SQL, Unit Testing,
NUnit, IIS (configuration and oepration), HTML(5), JavaScript, CSS(3), git.

### Responsibilities
  * Design and implementation of a new system for SLA management in Danske Bank.
    Tim has been sole developer on the project and has had the responsibility
    for both back- and frontend development. The backend is developed with
    C#/.NET by means of the ASP.NET MVC framework. The frontend is written in
    HTML, CSS, and JavaScript and is rendered serverside by means of Microsoft's
    Razor enginge. The web application is running on an IIS 8.0, which Tim has
    configured. The system's data store is an MS SQL Server, supplied by the
    internal organisation.

  * Continuous dialog with the business stakeholders in order to align
    expectations and clarify needs for, and requirements to, the system. The
    first version of the system was released on January 1st 2016 and has
    received continuous updates i 2016.

  * Migration of existing contracts from an old system to the newly developed
    one.

  * Integration to information about Danske Bank's HR and organisational
    structure. Information about employees and their relations to departments
    are continuously imported to the new SLA system to provide relevant accesses
    for e.g. managers to departments.

  * Integration to Danske Bank's product catalog which is the foundation for an
    SLA agreement. The product katalog is also continuously updated against the
    system in which the maintenance of these products are conducted.

  * Tim has written unit tests for big parts of the system.

## KMD / October 2014 - July 2015
Tim has been an external consultant in KMDs division for Citizen-near Software
Solutions, where he has developed a citizen facing self-service solution, that
enables citizens to apply for transportation reimbursements regarding for
instance doctor's visits. Tim has been the only C# developer on the project and
has worked with Polish developers, who has delivered the solution's SAP
component, and Danish business developers who has formulated the task.

### Technologies
HTML(5), JavaScript, CSS(3), bootstrap, jQuery, RequireJS, C#, .NET, ASP.NET
MVC, Unit Testing, NUnit, SAP integration, Borger.dk.

### Responsibilities

  * Development of domain models in C#/.NET that for instance supports
    validations involved in the application process.

  * Development of a configuration tool that makes it possible to define
    parameteres for e.g. validation rules in the backend. The self-service
    solution is really a flexible framework that can be configured for each
    municipality. Tim has documented the possibilities so that KMD's operations
    personnel can handle the configuration.

  * Development of a frontend in html5, css, and JavaScript. The frontend
    enables documentation of expenses by allowing the user to take pictures of
    receipts with a webcam or phone camera. The frontend is made so that all
    functionality works in Internet Explorer 8 and better.
  
  * Development af C#/.NET backend integrations to SAP, which is the system
    that municipality case workers use to process applications.
  
  * Integration with borger.dk, a portal for many municipality-related tasks
    that Danish citizens use with their digital signature, NemID.

  * Additionally, Tim has been tasked with being a buddy and mentor for newly
    hired KMD employee.

## Mailreminder.net / July 2014 - ?
Tim has developed mailreminder.net ---  a service that makes it easy for users
to be reminded about a given mail on a specified tome, without the service at
any time seeing the actual e-mail content - privacy is paramount. The service
can for instance be used to get flight information on top of the inbox on the
day of a trip.

### Technologies
HTML(5), JavaScript, Backbone, CSS(3), underscore, jQuery, RequireJS, grunt,
gulp, livereload, FreeBSD, Node.js, Express, MongoDB, PostgreSQL, SSL, postfix,
Dovecot, NGINX, supervisord, test-driven development.

### Responsibilities
Tim has developed a backen for mailreminder.net, that's comprised of:

  * A REST API, written in JavaScript by means of Node.js, Express, and MongoDB.
    The API sends and receives SSL encrypted JSON messages.

  * A mail server by means of postfix (as the MTA) and dovecot (as IMAP server),
    which is configured with account information stored in a PostgreSQL
    database.

  * A mail client, written in JavaScript by means of Node.js, that checks
    incoming mails and sets up a suitable reminder for those mails.

  * A job running tool which makes sure to send reminders at the correct times.

Tim has also developed a frontend where users can see which reminders have been
set up. The frontend is comprised of:

  * A "Single Page Application", written in JavaScript in the MVC patten by
    means of Backbone.js, underscore.js and RequireJS.

  * Styling by means of Sass stylesheets that are compiled into css.

  * Build scripts that, among other things, minifies and uglifies code and
    optimizes images to shorten load times as much as possible.

All services involved in running mailreminder.net are run on a FreeBSD server
which Tim administers.

## NHL Data / June 2014 - July 2014
Tim has been external consultant at NHL Data where he consulted in connection
with the start-up of a frontend for one of NHL's new products.

### Technologies
HTML(5), CSS(3), JavaScript, Backbone, underscore, jQuery, requirejs, grunt,
Live reload, C# .NET webservices, Scrum.

### Responsibilities
Tim has advised on the structuring of a JavaScript application for one of NHL's
new products. Tim has:

  * Developed a skeleton application in the MVC/MVT pattern by means
    backbone.js, underscre and RequireJS. NHL's developers have used the
    application as inspiration and further development.
  
  * Developed underscore templates.

  * Developed integrations with a C#/.NET backend through JSON messages.

## Danske Bank / august 2013 - maj 2014
Tim has been external consultant at Danske Bank Credit Management. Here Tim has
been involved in the development of web application for a new "impairment
system", in which case workers can outline a number of scenarios for possible
outcomes of a customer’s bankruptcy, and the consequences for the bank.

### Technologies
C#, .NET, ASP.NET, HTML, CSS, JavaScript (jQuery, Backbone and underscore), Live
reload, Unit test, integration til mainframe, Scrum.

### Responsibilities
  * Development of a frontend in HTML, CSS, and JavaScript that communicates
    with a C#/.NET backend through AJAX with JSON payloads. In the frontend,
    it’s possible for case workers to input expected losses for accounts and
    loans under certain circumstances (scenarios) and, at the same time, see the
    overall consequences for the bank for each scenario, and a mean consequence
    given the scenario probabilities.

  * Development of a C#/.NET backend application that communicates with the
    frontend and a number of mainframe programs through webservices in order to
    show, as the case worker inputs expected losses, the economical consequences
    for the bank given those losses.

  * Development of integrations for document generation based on the given
    input.  The documents are used for management reporting and accounting.

  * Specification of webservices that the mainframe need to expose in order to
    fullfil Danske Bank’s requirement of a fluent user interface.

  * Specification of tasks to Indian developer, who worked on another part of
    the user interface.

  * Technical documentation of the whole system stack: C#/.NET backend,
    JavaScript frontend, integrations between backend and frontend, and
    integrations between backend and mainframe.

## Edlund / February 2013 - August 2013
Tim has been employed by Edlund, where he has worked with INPAS, which is
Edlund's standard system for non-life insurance. Tim has had tasks within design
and development of regulation insurances, and analysis, design, and
development/corrections on INPASS webservices that are used when converting new
customers to the system.

### Technologies
C#, .NET, T-SQL, Windows Presentation Foundation, Reflection, NUnit, Unit test,
Test-Driven Development, Remoting, Scrum.

### Responsibilities
  * Development of data model that is the foundation for regulation insurances
    in C#/.NET, T-SQL, .NET remoting, and WPF. Regulation insurances are used
    by, among others, businesses with a fluctuating stockpile of goods, which
    means their premium shall be continuously regulated.

  * Analysis, design and development of web services (and corrections to
    webservices) that are used in connection with migrating 7 new non-life
    insurance providers to INPAS.

  * Analysis, design and specification of tasks for Sri Lankan development team
    and subsequent inspection and testing of the delivered source code.  Test of
    both regulation insurances and migration software through both unit- and
    integration testing.

  * Technical documentation of regulation insurances.

## Boligkontoret Danmark / February 2012 - February 2013
Consultant at Netcompany: Analysis, design, and development of ECM system for
BDK, which has extended task management features.

### Technologies
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Responsibilities
  * Development of migration software i C#/.NET for migrating BDK’s departments,
    tenants and other stakeholders.

  * Development of SharePoint webparts in HTML, CSS, JS, and ASP.NET.

  * Integration of ERP data in SharePoint that is used for case- and document
    taxonomy and task assignments.

  * Sizing and setup of server infrastructure for test- and production
    environments with MS SQL servers, SharePoint servers, and load balancing.

  * Training of approximately 30, both technical and business minded, key
    persons in features and use of the system in preparation for implementing
    and anchoring the system in the organisation.

## Københavns Almennyttige Boligselskab / January 2012 - February 2013
Consultant at Netcompany: Analysis, design, and development of ECM system for
KAB. The system, among other things, integrations with KABs Oracle ERP system
and their self-service portals. KAB is a non-profit housing organisation who
manages the operation of about 1000 housing departments distributed among 500
housing associations, which accomodates about 50,000 tenants and their families.

### Technologies
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Responsibilities
  * Development of integration to Oracle ERP system in C#/.NET by means of web
    services. The ERP data is integrated into SharePoint, where they are used
    for case- and document taxonomy among other things.

  * Development of task management system i C#/.NET and SharePoint, where task
    list templates defines which role a given should be assigned. ERP data are
    used to determine which person fulfils a given role for the housing
    department, for which the task list is added.

  * Development of SharePoint webparts in HTML, CSS, JavaScript, and ASP.NET,
    where it is possible to see a role crew for a housing department, among
    other things.

  * Development of integrations for KAB’s self-service portals enabling, for
    example, electronic mail to board members in connection with their work.

  * Sizing and setup of server infrastructure for test- and production
    environments with MS SQL servers, SharePoint servers, and load balancing.

## Danmarks Almennyttige Boligselskab / October 2011 - January 2012
Consultant at Netcompany: DAB have, in continuation of their ECM implementation,
expressed a desire to increase the collaboration for department- and
association board members. The desire is realised through a SharePoint portal
where board members can log in with NemID and collaborate on projects. DAB also
utilises the solution to send mail to board members - a function that saves
around 1 million danish kroner in postage yearly.

### Technologies
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint

### Responsibilities
  * Develepment of integrations to financial system i C#/.NET through
    webservices that for istance makes it possible for board members to browse
    account postings.
  
  * Development of user interface for displaying account postings in HTML, CSS,
    JavaScript and ASP.NET.

  * Development of tools for creation of "Board Rooms" that honor the
    confidentiality requirements that state that DAB may not have access to any
    board's documents and case files.

  * Training of 3 new employess in technical aspects of the product.

## DAHL Advokater / August 2011 - October 2011
Consultant at Netcompany: DAHL Advokater is one of Denmark's largets lawforms
with around 200 employees. DAHL, together with EG Data Inform, has devised a
vision for DAHL's ECM system going forward.

### Technologies
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint

### Responsibilities
  * Tim has produced a prototype system and solution description based on a
    requirements specification to get the project going.

  * Development of proof of concept integrations to DAHL's Navision system in
    C#/.NET.

  * Specification of development tasks that are delegated to a Netcompany Senior
    Developer, who has asked Tim for advice with regards to C#/.NET, HTML, CSS,
    JavaScript and SharePoint.

  * Education of about 20 persons in the developed prototype to visualise the
    functionality, practical use, and future potential.

## ACCURA / June 2011 - June 2011
Consultant at Netcompany: ACCURA advise a number of big national and
multi-national business clients, financial institutions, private equity funds,
state-owned investment funds, utility companies, public authorities, governments
and successful private companies, their owners and certain persons with large
netto fortunes. ACCURA has an ECM solution that handles theirs documents and
cases and the knowledge that's generated during their consulting.

### Technologies
C#, .NET, T-SQL, HTML, CSS, JS, SharePoint

### Responsibilities
After deployment of ACCURA's ECM solution, som need for adjustments arose and
the project's developers fell a bit behind schedule. Tim's responsibility was to
develop solutions to ACCURA's change requests and catch up with the schedule to
meet the agreed-upon deadline.

## Dansk Almennyttigt Boligselskab / October 2010 - October 2011
Consultant at Netcompany: DAB is a non-profit housing association that handles
the operation of about 100 housing companies with about 1,000 departments, that
house about 150,000 tenants and their families.

Tim has been the driving force in the development of GO Housing; an expansion to
the GetOrganized ECM system that makes it possible for DAB to handle casework at
lower cost and quicker with better service for the many departments and
residents.

### Technologies
C#, .NET, T-SQL, ASP.NET, HTML, CSS, JS, SharePoint, Network Load Balancing

### Responsibilities
  * Gathering of requirements through a nubmer of workshops where Tim among
    other things has been responsible for presentation materials and minutes.

  * Development of integrations to ERP system in C#/.NET. The ERP system
    delivers information used, for instance, in connection with case- and
    document taxonomies.

  * Specification, development, and test of a task assignment system in C#/.NET.
    The system enables assignment of tasks to persons according to a
    department's employees and their roles.

  * Development of webparts for SharePoing in HTML, CSS, JavaScript and ASP.NET
    that, among other things, enables personnel to gain a quick overview a
    department's employees and their roles.

  * Development of tool to convert departments, members, and tenants in C#/.NET.

  * The system is relatively complex and has pushed SharePoint to its limits,
    which is why Tim has headed up identification and corrections to
    hard-to-identify problems in SharePoint.

  * Sizing and configuration of server infrastructure for test- and production
    environments with Microsoft SQL Servers, SharePoint Servers and load
    balancing servers.

## Aalborg Universitet / July 2008 - August 2008
Aalborg University wanted to have a prototype of software that could control
Home Automation Systems by means of for instance a phone's GPS system. In 2008,
Home Automation Systems was not something  controlled via a touch interface - if
it was controlled through a phone it happened through oldfashioned sms, but more
often it happened on infrared remotes. Tim developed a REST API that exposed a
home automation system's functionality so it could be used from a phone with a
GPS. The work became a starting point for Tim's speciality in programming
technologies and embedded systems at the university.

# Education

-------------------------------------------------------------------------------
Period        Title
------------- -----------------------------------------------------------------
2008 - 2010   Cand.Polyt. in Software (softwareingeniør), Aalborg Universitet.
							SPecialized in programming technologies and embedded systems. 

2004 - 2008   Bachelor, BSc in Software, Aalborg Universitet.

2000 - 2003   Student, Higher Technical Exam (HTX), Struer.

-------------------------------------------------------------------------------

# Other Work Experience

-------------------------------------------------------------------------------
Period        Occupation
------------- -----------------------------------------------------------------
2004 - 2005   Conscript in the engineering corps in Skive,
              [APC](https://en.wikipedia.org/wiki/Armoured_personnel_carrier) 
              driver.

-------------------------------------------------------------------------------

# Languages
Proficient Danish and English both verbally and in writing.

# Skills
+------------+----------------------------------+-------------+-------+--------+
| Category   | Skill                            | Level       | Years | Latest |
|            |                                  |             |       | use    |
+============+==================================+=============+=======+========+
| General    | Agile methodologies              | Experienced | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Architecture                     | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Backend                          | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | User interface developement      | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Documentation                    | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | ECM (Electronic Case- and        | Very        | 3     | 2013   |
|            | Document Management              | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Talking with business people and | Very        | 5     | 2015   |
|            | participate in workshops         | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Frontend                         | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Implementation                   | Experienced | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Conversion                       | Very        | 4     | 2014   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Requirement specification        | Experienced | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | OOA (Object Oriented Analysis)   | Experienced | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | OOD (Object Oriented Design)     | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | OOP (Object Oriented             | Very        | 5     | 2015   |
|            | Programming)                     | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Programming                      | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Scrum                            | Experienced | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Systems analysis                 | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Systems design                   | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Systems development              | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| General    | Development methodologies        | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Web services                     | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| General    | Web development                  | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | C#                               | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | CSS                              | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | HTML                             | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | JavaScript                       | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | Python                           | Experienced | 1     | 2012   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | SQL                              | Experienced | 4     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Language   | Transact-SQL                     | Experienced | 4     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | .NET                             | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | Ajax                             | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | ASP.NET                          | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Technology | LINQ                             | Very        | 4     | 2014   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Technology | Skade.Net (Edlund)               | Experienced | 1     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | Windows Communication Foundation | Experienced | 2     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | XML                              | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Technology | Web                              | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | Web services                     | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Technology | SharePoint 2010 (webparts,       | Expert      | 3     | 2013   |
|            | timerjobs, event handlers, etc.) |             |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Technology | REST                             | Very        | 3     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Microsoft SQL Server             | Experienced | 3     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Microsoft SQL Server 2008        | Experienced | 3     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Backbone.js                      | Expert      | 2     | 2014   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Bootstrap                        | Experienced | 3     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Gruntjs                          | Experienced | 1     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | jQuery                           | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Microsoft Visual Studio          | Expert      | 5     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Node.js                          | Experienced | 2     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Nunit                            | Very        | 3     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Require.js                       | Very        | 2     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | underscore.js                    | Expert      | 2     | 2015   |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | SharePoint 2010 (installation,   | Expert      | 3     | 2013   |
|            | operation, load balancing etc.)  |             |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | SVN / Git / TFS                  | Very        | 5     | 2015   |
|            |                                  | experienced |       |        |
+------------+----------------------------------+-------------+-------+--------+
| Dev. tool  | Gemini                           | Good        | 1     | 2014   |
|            |                                  | knowledge   |       |        |
+------------+----------------------------------+-------------+-------+--------+

<!-- vim: set ts=2 sw=2 tw=80 et : -->
